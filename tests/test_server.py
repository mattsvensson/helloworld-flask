#!/usr/bin/env python3
import re
import pytest


##########################################################
# Start server 
##########################################################
from tests.http_server import ServerThread, make_url
server = ServerThread()
server.setup()
server.start()


##########################################################
# Request based
##########################################################
import requests
session = requests.session()
username = "test"
email = "test@here.com"
password = "password"

def get_csrf_token(source_code):
    return re.findall('name="csrf_token" type="hidden" value="(.+?)"', source_code)[0]

def test_homepage():
    """Ensure the homepage loads"""
    r = session.get(make_url('index'))
    assert r.status_code == 200

def test_ip_lookup():
    """Ensure the IP lookup loads"""
    r = session.get(make_url('get_client_ip'))
    assert r.status_code == 200   

def test_private_page_redirect():
    """Ensure the private page redirects to login"""
    r = session.get(make_url('private'))
    assert 'Please sign in' in r.text   

def test_signup_page():
    """Ensure the signup page loads"""
    #Get CSRF Token
    r = session.get(make_url('signup'))
    assert r.status_code == 200  
    csrf_token = get_csrf_token(source_code=r.text)

    # Login
    r = session.post(make_url('signup'), json={"username":username,"email":email,"password":password,"confirm":password,"csrf_token":csrf_token})
    assert 'Private Data for %s' % (username) in r.text

def test_logout():
    """Ensure logout works, redirecting you to sign in page on private pages"""
    # Logout
    r = session.get(make_url('logout'))
    # Try to get to private page
    r = session.get(make_url('private'))
    assert 'Please sign in' in r.text

def test_login_requests():
    """Ensure the login process works"""
    # Get CSRF Token
    r = session.get(make_url('login'))
    assert r.status_code == 200  
    csrf_token = get_csrf_token(source_code=r.text)

    # Login
    r = session.post(make_url('login'), json={"username":username, "password":password, "csrf_token":csrf_token})
    assert 'Private Data for %s' % (username) in r.text

def test_delete_test_user():
    import sqlite3
    from app.run_server import user_db_file
    con = sqlite3.connect(user_db_file)  
    cur = con.cursor()
    cur.execute("delete from user where username='%s';" % (username))
    con.commit()


##########################################################
# Selenium based
##########################################################
from selenium.webdriver.firefox.options import Options
from seleniumrequests import Firefox
import signal

#Create a single server thread, client and browser
opts = Options()
opts.headless = True
browser = Firefox(options=opts)
browser.set_page_load_timeout(5)

def test_datatables_static_page():
    """Ensure the static datatable loads"""
    browser.get(make_url('datatables_static'))
    assert 'Alvarado Whitley' in browser.page_source

def test_datatables_dynamic_page():
    """Ensure the dynamic datatable loads"""
    # Be sure the page loads
    browser.get(make_url('datatables_dynamic'))
    assert 'Load the Table!' in browser.page_source
    # Be sure the datatable loads
    browser.find_element_by_css_selector('.loadbutton').click()
    assert 'Alvarado Whitley' in browser.page_source

def test_highcharts():
    """Ensure the Highcharts load"""
    browser.get(make_url('highcharts'))
    assert 'Solar Employment Growth by Sector' in browser.page_source

def test_highmaps():
    """Ensure the Highmaps load"""
    browser.get(make_url('world_map'))
    assert 'Created with Highmaps' in browser.page_source

def test_close_selenium():
    browser.service.process.send_signal(signal.SIGTERM)
    browser.quit()

##########################################################
# Stop server
##########################################################
def test_close_server():
    server.stop()