#!/usr/bin/env python3
from app.run_server import app as _app, url_for
import threading
import wsgiref.simple_server
TEST_SERVER_PORT = 5000
_app.config['SERVER_NAME'] = 'localhost:{port}'.format(port=TEST_SERVER_PORT)


class ServerThread(threading.Thread):
    def setup(self):
        _app.config['TESTING'] = True
        self.port = TEST_SERVER_PORT

    def run(self):
        self.httpd = wsgiref.simple_server.make_server('localhost', self.port, _app)
        self.httpd.serve_forever()

    def stop(self):
        self.httpd.shutdown()

##########################################################
# Helpers
##########################################################
def make_url(endpoint, **kwargs):
    with _app.app_context():
        return url_for(endpoint, **kwargs)