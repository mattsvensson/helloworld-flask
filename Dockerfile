###############################################################################################
#
# Build: sudo docker build -t flaskwebpage .
#
# Build: sudo docker run -d -p 80:8000 --restart=always -t --name flaskwebpage flaskwebpage
#
###############################################################################################

FROM alpine:latest

#Install python3 and pip3
RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache

#Copy and install requirements
RUN pip3 install Flask requests flask_bootstrap flask_wtf flask_sqlalchemy flask_login

#Copy the server code
ADD app /home/flaskwebpage/app

#Start the server
CMD ["python3", "/home/flaskwebpage/app/run_server.py"]

