#!/usr/bin/env python3
from flask import Flask, render_template, request, redirect, jsonify, url_for
import os, sys, traceback
import datetime
import time
import json
import requests
from flask_bootstrap import Bootstrap
from scripts.nocache import nocache
from scripts.get_exception import get_exception
from flask_sqlalchemy  import SQLAlchemy
from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, PasswordField, BooleanField, TextAreaField, validators
from wtforms.validators import InputRequired, Email, Length
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
user_db_file = "/home/mattsvensson/Dropbox/it/bitbucket/helloworld-flask/app/user.db"

#Get 'er running!
app = Flask(__name__)
app.config['SECRET_KEY'] = 'Thisissupposedtobesecret!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///%s'%(user_db_file)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['RECAPTCHA_PUBLIC_KEY']='6LeFAqAUAAAAAAif9Jt-X0MpPGrUsGUvOCiwdgXO'
app.config['RECAPTCHA_PRIVATE_KEY']='6LeFAqAUAAAAAIqNW4xIptIysMKbpM7XCk8G1tiw'
app.config['RECAPTCHA_OPTIONS']= {'theme':'black'}
db = SQLAlchemy(app)
bootstrap = Bootstrap(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


################################################################
#  Auth
################################################################
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
    remember = BooleanField('remember me')
    error = ""

class InvalidLoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
    remember = BooleanField('remember me')
    error = "Invalid sername and/or password"

class RegisterForm(FlaskForm):
    email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
    password = PasswordField('New Password', [
            validators.DataRequired(),
            validators.EqualTo('confirm', message='Passwords must match')
        ])
    confirm = PasswordField('Repeat Password')
    recaptcha = RecaptchaField()
    error = ""

class RegisterFormUserExists(FlaskForm):
    email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
    password = PasswordField('New Password', [
            validators.DataRequired(),
            validators.EqualTo('confirm', message='Passwords must match')
        ])
    confirm = PasswordField('Repeat Password')
    error = "User already exists"


################################################################
#  Home page
################################################################
@app.route("/") 
@app.route("/index")
@nocache
def index():
    try:
        return render_template('index.html')
    except:
        return get_exception()


################################################################
#  Login
################################################################
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        print("didn't find %s: %s" % (form.username.data, user))
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                return redirect('private')
        return render_template('protected/login.html', form=InvalidLoginForm())

    return render_template('protected/login.html', form=LoginForm())


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegisterForm()

    if form.validate_on_submit():
        if User.query.filter_by(username=form.username.data).first():
            return render_template('protected/signup.html', form=RegisterFormUserExists())
        else:
            hashed_password = generate_password_hash(form.password.data, method='pbkdf2:sha512')
            new_user = User(username=form.username.data, email=form.email.data, password=hashed_password)
            db.session.add(new_user)
            db.session.commit()
            login_user(new_user, remember=False)
            return redirect('private')

    return render_template('protected/signup.html', form=form)


@app.route('/private')
@login_required
def private():
    return render_template('protected/private.html', name=current_user.username)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


################################################################
#  HighMaps
################################################################
@app.route("/world_map", methods=['GET'])
@nocache
def world_map():
    ''' 
        Load the world map with the source and destination files requested, e.g. 
        site/world_map?src_file=sources.json&dst_file=destinations.json

        Note: it is assumed the files are in the app/static/json folder
    '''
    try:
        src_file = request.args.get('src_file')
        dst_file = request.args.get('dst_file')
        return render_template( 'highmaps/worldmap.html', 
                                                map_title="Example Source+Dest Map",
                                                map_subtitle="Random Data",
                                                #First group
                                                map_data_link_src="/get_json_data?file={src_file}".format(src_file=src_file),
                                                series_color_src="#ff1a1a",
                                                #Second group
                                                map_data_link_dst="/get_json_data?file={dst_file}".format(dst_file=dst_file),
                                                series_color_dst="#2ef612",
                                                refresh_interval_ms=60000,      #1000 == one second
                              )
    except:
        return get_exception()


################################################################
# Highcharts
################################################################
@app.route("/highcharts")
@nocache
def highcharts():
    try:
        return render_template('highcharts/highcharts.html')
    except:
        return get_exception()


################################################################
# Data tables
################################################################
@app.route("/datatables_static")
@nocache
def datatables_static():
    try:
        return render_template('datatables/datatables_static.html')
    except:
        return get_exception()
@app.route("/datatables_dynamic")
@nocache
def datatables_dynamic():
    try:
        return render_template('datatables/datatables_dynamic.html')
    except:
        return get_exception()
@app.route("/software_list")
@nocache
def software_list():
    try:
        return render_template('datatables/software_list.html')
    except:
        return get_exception()

################################################################
# Show a client their IP
################################################################
@app.route("/get_client_ip")
@nocache
def get_client_ip():
    """Return a page showing the client's IP"""
    try:
        return render_template( 'get_client_ip.html', 
                                server_public_ip=get_server_public_ip(), 
                                client_request_ip=get_client_ip(), 
                                client_xforward_fors=get_client_xforward_for()
                              )
    except:
        return get_exception()




################################################################
# Error handling
################################################################
@app.errorhandler(404)
@nocache
def page_not_found(e):
    """Return a custom 404 error page"""
    try:
        return render_template('errors/404.html'), 404
    except:
        return get_exception()
@app.errorhandler(500)
@nocache
def exception_handler(e):
    """Return a custom 500 error page"""
    try:
        return render_template('errors/500.html'), 500
    except:
        return get_exception()


################################################################
# Helper functions
################################################################
@app.route("/get_json_data", methods=['GET'])
@nocache
def get_json_data():
    """Load a given static JSON file and return it"""
    try:
        return app.send_static_file('json/{file}'.format(file=request.args.get('file')))
    except:
        return get_exception()


################################################################
# Check for and be able to block IP addresses
################################################################
def get_client_ip():
    try:
        return request.remote_addr
    except:
        return get_exception()


def get_client_xforward_for():
    """Gets client IP from xforward for or remote address if not proxied"""
    try:
        return request.headers.getlist("X-Forwarded-For")
    except:
        return get_exception()


def get_server_public_ip():
    try:
        return requests.get('http://ipv4.icanhazip.com').text.strip()
    except:
        return get_exception()
        

@app.before_request
def check_blocked_ips():
    """Before making a request, check if the IP is blocked.  If so, show a blocked picture"""
    if get_client_ip() in ["192.168.86.44"]:
        return app.send_static_file('img/youve-been-blocked_o_1234100.jpg')


if __name__ == '__main__':
      app.run(host='0.0.0.0', port=8000, debug=True)
